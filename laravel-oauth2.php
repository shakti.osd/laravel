

Setting up OAuth2 Server
To set up the OAuth server, first we will create a new Laravel 5.3 project.

laravel new passport-server

Now update the database credentials in .env file.

DB_HOST="127.0.0.1"
DB_PORT=3306
DB_DATABASE="laravel"
DB_USERNAME="root"
DB_PASSWORD="root"
Let's also create the Auth scaffold

php artisan make:auth

Now we will install the Passport package using composer.

composer require laravel/passport

After installing the package, we need to register the Passport service provider in the providers array of config/app.php

Laravel\Passport\PassportServiceProvider::class

Now we are ready to run the database migrations.

php artisan migrate

After installing the package using composer, we need to run passport:install command, which will create the encryption keys required by the OAuth2 library. It will also create a personal access and password clients for us.

php artisan passport:install

Now add Laravel\Passport\HasApiTokens trait in to the App\User model.

<?php
// app/User.php
namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;
}

Update the authentication guard driver for api in config/auth.php

    'guards' => [
        'web' => [
            'driver' => 'session',
            'provider' => 'users',
        ],

        'api' => [
            'driver' => 'passport',
            'provider' => 'users',
        ],
    ],
    
    
Register Passport routes in AuthServiceProvider provider.

use Laravel\Passport\Passport;

public function boot()
    {
        $this->registerPolicies();
        Passport::routes();
        //
    }
    


make PassportController controller in /app/Http/Controllers/API/PassportController.php

<?php
namespace App\Http\Controllers\API;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Validator;
class PassportController extends Controller
{
    public $sucessStatus = 200;
    
    /*
     * login api
     * 
     * @return \Illuminate\Http\Response
     */
    
    public function login() {
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])) {
            $user = Auth::user();
            $success['token'] = $user->createToken('MyApp')->accessToken;
            return response()->json(['success' => $success], $this->sucessStatus);
        }
        else {
            return response()->json(['error' => 'Unauthorised'], 401);
        }
    }
    
    /*
     * Register api
     * 
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request) {
        $validator = Validator::make($request->all(),[
            'name' => 'required|unique:users',
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'c_password' => 'required|same:password',
        ]);
        
        if($validator->fails()) {
            return response()->json(['error' => $validator->errors()],401);
        }
        
        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $user = User::create($input);
        $success['token'] = $user->createToken('MyApp')->accessToken;
        $success['name'] = $user->name;
        
        return response()->json(['success' => $success], $this->sucessStatus);
    }
    
    /*
     * details api
     * 
     * @return \Illumiante\Http\Response
     */
     public function getDetails() {
         $user = Auth::user();
         return response()->json(['success' => $user], $this->sucessStatus);
     }
            
}
    
    
Run from Postman

Register (Fields depends on users table)
==========
Request Url: http://codehacking.local/api/register
In body
email:demo@demo.com
password: demo
cpassword: demo
name: demo
is_active: 1
photo_id: 1

{"success":{"token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImRmYzhjMzBjMmRmNDlmNjkwNzFmMjViYWVkMzNkMTcwYTEzOTBmY2MzYjVjZTg5ZTU4MmJhODQ5MGVjN2NkMmE3MmU5N2Q2MzcxNjg3MGYxIn0.eyJhdWQiOiIxIiwianRpIjoiZGZjOGMzMGMyZGY0OWY2OTA3MWYyNWJhZWQzM2QxNzBhMTM5MGZjYzNiNWNlODllNTgyYmE4NDkwZWM3Y2QyYTcyZTk3ZDYzNzE2ODcwZjEiLCJpYXQiOjE1MzAxODQ2MTEsIm5iZiI6MTUzMDE4NDYxMSwiZXhwIjoxNTYxNzIwNjExLCJzdWIiOiI3Iiwic2NvcGVzIjpbXX0.tr3wME7XAPUd8tp4wLvffvl8xPNDfU3LetwpJs9hCBZeRDh1SBbItrZcgA0GzSoxZ7p4dHVzc3L6CCr9ya4to2TxUMOPnHUph4x1-eDu6r1WcrHrPyLVGlXq-ndtpZzcCMATFNaBjtyXAWnP0YO5v7pYXrJJeNMyka_p84aWl8SgN80v2JoNZbkdj8-_qzEzRufCILoRy_DLqEGDCXG8cwj8pqv4sLWOHYBDGX_bChgtMTDERiIg8-BI6e0vrIW4JEmbAijyswHrOjM49gtGTaQ_zY2AqJ2VXvb6r9UugK6AuxqYEj2FH6e85V2sftW3oypmpjPueUSc7qr_8AfCYJmaDnBuyCBGa7HAdue6AbiXx8PFS65HNUKtIO_kzPYttgPEBLMkl2hrxY21Dq7xWMSAF3Une27m-uLSsKVM1JFG6GkF4HwEjbN1jMfEsO1VwuTNLz6zFTNE5gPZE84cRJ6ex54OavFESWy_Ik8OJmHYdmPEp_NfJ02TAoLRsN4fix9AFjWWvR_oh3b2fV4ZrRuF8Ce13Eejnw9MVvyEgwj7rVlS7slW3ArMCf4ih5JBXF2QE3o3aFk3xVR7JLFnXuVXNM3dCdM5FZAJAiDejMqJ-SZlBcXFtESHxGMevfoQ2PdgDXmEdinN0L6pmzw0alRymjK3S0KJJhAuAVGPDUw","name":"demo"}}


Login
=========
Request Url: http://codehacking.local/api/login
In body
Email:demo@demo.com
Password: demo



{"success":{"token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjgxMTVlYzY0YzViODk3Nzk0YTkzNjdkZDMwMTBmZjFkNTA4NDUwZDFjMDQzNGMxMjZlNzAxOTFiNzc0ODE1NjQyYzMzNzdlNzlhZTcyMjQ1In0.eyJhdWQiOiIxIiwianRpIjoiODExNWVjNjRjNWI4OTc3OTRhOTM2N2RkMzAxMGZmMWQ1MDg0NTBkMWMwNDM0YzEyNmU3MDE5MWI3NzQ4MTU2NDJjMzM3N2U3OWFlNzIyNDUiLCJpYXQiOjE1MzAxODQ3NjAsIm5iZiI6MTUzMDE4NDc2MCwiZXhwIjoxNTYxNzIwNzYwLCJzdWIiOiI3Iiwic2NvcGVzIjpbXX0.JHJFoGHsGsoJsDd7JwPddk4Ljar8IjPm2zpjjmb-5RZJF1BHXpy0MkSVO3YDAlhla3wT_gDj_6fm-S2nfwJ0pQgt1x61rjSnGzAR7Je04Pfjsk7tuMZGij85T-0XDcg02dCG4-huHWjpeCdgThy8nWkVS-pkA1wEQScMTcEYUBLBSpcjLlVu5HlCsT9wN17TyBZsT12O-UONuWGg-oSnz0TYFKz0YBWZ9x88aHEueiLTbJFkmZ7v5YrDafsiNX_Y9i6iBIgchWWUpRWqjPCj96rzavayp-J0mgCCjevbGvbHYUbFjNZvOLIQvRYl2_3IuFlnWVgHjEbV2Uf1RfQHk9kztM5TZh2YA0EROj9e2GpolDukxs_4IeMX_LSlI1AO0cuaqHfeN_FjSHw0X5LaL6Hi7Z6zU6DIPEfmvVsVRSfkEsWCbps79MLg5OX_17dJrpzza9Ah1se6SZ3SPLVAytv6fou3pcB8xQ8cDTYBysUx26xe-u2-0PuEp_u5gTgyirFKqn0Vuvom2UPjJjziuWktmt4XVvcLvOOO_IZqLofHHzj3sOQS3y4f9QFXffLOlg6SdiQPHkNvjtfhcRvLdsSIk-jwOFlq9zDYd0P-Nn6K39HGswPSszktiFZLbJIxY8CkvE6Ul6t_KalrLJ34iHmaQ1loQomGUpDftPrEuWc"}}

Get Details
===============
Request Url: http://codehacking.local/api/get-details
In header
Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjgxMTVlYzY0YzViODk3Nzk0YTkzNjdkZDMwMTBmZjFkNTA4NDUwZDFjMDQzNGMxMjZlNzAxOTFiNzc0ODE1NjQyYzMzNzdlNzlhZTcyMjQ1In0.eyJhdWQiOiIxIiwianRpIjoiODExNWVjNjRjNWI4OTc3OTRhOTM2N2RkMzAxMGZmMWQ1MDg0NTBkMWMwNDM0YzEyNmU3MDE5MWI3NzQ4MTU2NDJjMzM3N2U3OWFlNzIyNDUiLCJpYXQiOjE1MzAxODQ3NjAsIm5iZiI6MTUzMDE4NDc2MCwiZXhwIjoxNTYxNzIwNzYwLCJzdWIiOiI3Iiwic2NvcGVzIjpbXX0.JHJFoGHsGsoJsDd7JwPddk4Ljar8IjPm2zpjjmb-5RZJF1BHXpy0MkSVO3YDAlhla3wT_gDj_6fm-S2nfwJ0pQgt1x61rjSnGzAR7Je04Pfjsk7tuMZGij85T-0XDcg02dCG4-huHWjpeCdgThy8nWkVS-pkA1wEQScMTcEYUBLBSpcjLlVu5HlCsT9wN17TyBZsT12O-UONuWGg-oSnz0TYFKz0YBWZ9x88aHEueiLTbJFkmZ7v5YrDafsiNX_Y9i6iBIgchWWUpRWqjPCj96rzavayp-J0mgCCjevbGvbHYUbFjNZvOLIQvRYl2_3IuFlnWVgHjEbV2Uf1RfQHk9kztM5TZh2YA0EROj9e2GpolDukxs_4IeMX_LSlI1AO0cuaqHfeN_FjSHw0X5LaL6Hi7Z6zU6DIPEfmvVsVRSfkEsWCbps79MLg5OX_17dJrpzza9Ah1se6SZ3SPLVAytv6fou3pcB8xQ8cDTYBysUx26xe-u2-0PuEp_u5gTgyirFKqn0Vuvom2UPjJjziuWktmt4XVvcLvOOO_IZqLofHHzj3sOQS3y4f9QFXffLOlg6SdiQPHkNvjtfhcRvLdsSIk-jwOFlq9zDYd0P-Nn6K39HGswPSszktiFZLbJIxY8CkvE6Ul6t_KalrLJ34iHmaQ1loQomGUpDftPrEuWc
Accept: Application/json



Ref1: https://blog.shameerc.com/2016/08/set-up-oauth2-server-using-laravel-passport
Ref2: https://www.youtube.com/watch?v=r4qcbB8y3Vk
Ref3: https://github.com/RamakanthRapaka/Laravel-Api-Rest